package database

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

//DbConn exported variable to perform sql operations
var DbConn *sql.DB

//SetupDatabase opens a connection to the mysql database on "inventorydb"
func SetupDatabase() {
	dbUser := "root"
	dbPassword := "password"
	dbHostname := "127.0.0.1"
	dbPort := 3306
	dbName := "inventorydb"
	dbMaxOpenConnections := 4
	dbMaxIdleConnections := 4
	dbConnectionMaxLifetime := 60 * time.Second
	var err error
	DbConn, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", dbUser, dbPassword, dbHostname, dbPort, dbName))
	if err != nil {
		log.Fatal(err)
	}
	DbConn.SetMaxOpenConns(dbMaxOpenConnections)
	DbConn.SetMaxIdleConns(dbMaxIdleConnections)
	DbConn.SetConnMaxLifetime(dbConnectionMaxLifetime)

}
