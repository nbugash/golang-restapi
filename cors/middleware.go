package cors

import (
	"net/http"
)

//Middleware : setting up the middle to enable client to use the api
func Middleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request){
		responseWriter.Header().Add("Access-Control-Allow-Origin", "*")
		responseWriter.Header().Add("Content-Type", "application/json")
		responseWriter.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		responseWriter.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		handler.ServeHTTP(responseWriter, request)
	})
}
