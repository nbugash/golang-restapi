package receipt

import (
	"io/ioutil"
	"path/filepath"
	"time"
)

//ReceiptDirectory is the location of where the receipt files will be stored
var ReceiptDirectory = filepath.Join("uploads")

//Receipt model
type Receipt struct {
	ReceiptName string    `json:"name"`
	UploadDate  time.Time `json:"uploadDate"`
}

//GetReceipt reads receipts from the ReceiptDirectory
func GetReceipt() ([]Receipt, error) {
	receipts := make([]Receipt, 0)
	files, err := ioutil.ReadDir(ReceiptDirectory)
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		receipts = append(receipts, Receipt{
			ReceiptName: file.Name(),
			UploadDate:  file.ModTime(),
		})
	}
	return receipts, nil
}
