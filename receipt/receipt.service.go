package receipt

import (
	"encoding/json"
	"fmt"
	"gitlab.com/nbugash/golang-restapi/cors"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const receiptPath = "receipts"

func handleReceipts(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		receiptList, err := GetReceipt()
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		receiptJSONList, err := json.Marshal(receiptList)
		if err != nil {
			log.Fatal(err)
			return
		}
		_, err = res.Write(receiptJSONList)
		if err != nil {
			log.Fatal(err)
		}
		return
	case http.MethodPost:
		req.ParseMultipartForm(5 << 20) // 5Mb
		file, fileHandler, err := req.FormFile("receipt")
		if err != nil {
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		defer file.Close()
		f, err := os.OpenFile(filepath.Join(ReceiptDirectory, fileHandler.Filename), os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		f.Close()
		io.Copy(f, file)
		res.WriteHeader(http.StatusCreated)
		return
	case http.MethodOptions:
		return
	default:
		res.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

func handleDownload(res http.ResponseWriter, req *http.Request) {
	urlPathSegment := strings.Split(req.URL.Path, fmt.Sprintf("%s/", receiptPath))
	if len(urlPathSegment[1:]) > 1 {
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	filename := urlPathSegment[1:][0]
	file, err := os.Open(filepath.Join(ReceiptDirectory, filename))
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	defer file.Close()
	fileHeader := make([]byte, 512)
	file.Read(fileHeader)
	fileContentType := http.DetectContentType(fileHeader)
	fileStat, err := file.Stat()
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	fileSize := strconv.FormatInt(fileStat.Size(), 10)
	res.Header().Set("Content-Type", fileContentType)
	res.Header().Set("Content-Disposition", "attachment; filename="+filename)
	res.Header().Set("Content-Length", fileSize)
	file.Seek(0, 0)
	io.Copy(res, file)
}

//SetupRoutes : For setting up the routes for the receipt handler
func SetupRoutes(apiBasePath string) {
	receiptHandler := http.HandlerFunc(handleReceipts)
	downloadHandler := http.HandlerFunc(handleDownload)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, receiptPath), cors.Middleware(receiptHandler))
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, receiptPath), cors.Middleware(downloadHandler))
}
