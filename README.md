# Readme

## Required software

- GO (version 1.15)
- VSCode
- Postman
- Docker
- Mysql (using docker)

## Setting up development environments

### Installing golang

### Installing VSCode

### Installing Mysql

We're going to install mysql using docker. The command to quickly spin up a mysql docker container is

    $ docker run --name hockeyDB -e MYSQL_ROOT_PASSWORD=password -d mysql:latest -p 3306:3306

or by creating a docker-compose file

    version: "3.1"

    services:
      db:
        container_name: hockeyDB
        image: mysql
        command: --default-authentication-plugin=mysql_native_password
        restart: always
        volumes:
          - ./mysql/datadir:/var/lib/mysql
        ports:
          - 0.0.0.0:3306:3306
        environment:
          MYSQL_ROOT_PASSWORD: password
          MYSQL_DATABASE: hockeyDB

Note:
Make sure that there is a directory `infra/mysql/datadir`. Also the name of the container is `hockeyDB`, the password will be `password` and we will be using the latest version of mysql.

## Handling HTTP Requests

### Creating basic handlers

There are two ways to create an http handler
(1) By using http.Handle(<pattern>, <handler object>)
(2) By using http.HandleFunc(<pattern>, <handler function>)

#### Using a handler object

Create a handler struct with a Message field with a ServeHTTP function signature

```go
type fooHanlder struct {
    Message string
}

func (f* fooHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte(f.Message))
}
```

Now you can use this Handler

```go
:
func main() {
    http.Handle("/foo", &fooHandler{Message: "called foo"})
}
:
```

#### Using a handler function

Create a handler function with the correct parameter signature
(1) http.ResponseWriter
(2) http.Request

```go
func barHandler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("returing some string to the client"))
}

func main() {
    http.HandleFunc("/bar", barHandler)
}
```

### Working with JSON

In this section, we will using the `json.Marshal` and `json.Unmarshal` libraries provided by Go. We will use this to read and write http requests and response.

#### Marshalling a struct

(1) Create a struct type

```go
type Product struct {
	ProductID      int    `json:"productId"`
	Manufacturer   string `json:"manufacturer"`
	Sku            string `json:"sku"`
	Upc            string `json:"upc"`
	PricePerUnit   string `json:"pricePerUnit"`
	QuantityOnHand int    `json:"quantityOnHand"`
	ProductName    string `json:"productName"`
}
```

(2) Convert the struct to a byte size array

```go
func main() {
    product := &Product{
        ProductId: 123,
		    Manufacturer:"Big Box Company",
			  Sku:"4561qHJK",
		    Upc:"414654444566",
			  PricePerUnit:"12.99",
			  QuantityOnHand:28,
			  ProductName:"Gizmo"
    }

    productJSON, _ := json.Marshal(product)

    fmt.Println(string(productJSON))
}
```

#### Unmarshalling a string to a struct

Using the same struct from the previous example

```go
type Product struct {
	ProductID      int    `json:"productId"`
	Manufacturer   string `json:"manufacturer"`
	Sku            string `json:"sku"`
	Upc            string `json:"upc"`
	PricePerUnit   string `json:"pricePerUnit"`
	QuantityOnHand int    `json:"quantityOnHand"`
	ProductName    string `json:"productName"`
}

func main() {

	productJSON := `{
			"productId":123,
			"manufacturer":"Big Box Company",
			"sku":"4561qHJK",
			"upc":"414654444566",
			"pricePerUnit":"12.99",
			"quantityOnHand":28,
			"productName":"Gizmo"
			}`

	product := Product{}
	err := json.Unmarshal([]byte(productJSON), &product)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("json unmarshal product: %s", product.ProductName)
}
```

### Working with Request

Using the knowledge from [basic handlers](#creating-basic-handlers) and [working with JSON](#working-with-json), we are now in the position to receive http request for our api server

Looking deeper in the request object

(1) `Request.Method` which returns a `string`

(2) `Request.Header` whichh returns a `map[string][]string`. This is simply a map of strings (key) to a list of strings (value) - i.e in Java it'll be `Map<String, List<String>>`

(3) `Request.Body` which returns a `io.ReadCloser`. Returns a EOF when not present

First we need to define a new handler function. Next we'll create a `switch` block using `request.Method` as the control variable. Then for each case block it'll match the request method to one of the http functions / verbs

```go
func productHandler(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case http.MethodGet:
		productsJSON, err := json.Marshal(productList)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			log.Fatal(err)
		}
		writer.Header().Set("content-type", "application/json")
		writer.Write(productsJSON)
	case http.MethodPost:
		var newProduct Product
		bodyBytes, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		err = json.Unmarshal(bodyBytes, &newProduct)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		// client cannot set the PRODUCTID
		// it should be handled by the server
		if newProduct.ProductID != 0 {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		newProduct.ProductID = getNextProductID()
		productList = append(productList, newProduct)
		writer.WriteHeader(http.StatusCreated)
		return
	}
}
```

### URL Path pattern

We can use the request.URL type. Looking under the hood, the URL struct looks like this

    type URL struct {
        Schema string
        Opaque string
        User *Userinfo
        Path string        <<-- We can use this to determine the dynamic routes
        RawPath string
        ForceQuery bool
        RawQuery string
        Fragment string
    }

#### Static Routes

We define the static routes in the `http.HandleFunc`

```go
func main() {
    http.HandleFunc("/products", productsHandler)
    http.HandleFunc("/products/", productHandler) // note the trailing "/" in the pattern string and the handler used
}
```

#### Dynamic or Parametric Routes

To determine the dynamic routes used, we can utilize the `request.URL.path`. First we split the url into its path segment

    urlPathSegments := strings.Split(request.URL.path, "products/")

The last index of the urlPathSegments string array will be the `id` in the url `products/{id}`. Optionally, we convert the id into an `int`. Note: we are converting the id into an int because Product.ProductID is set to be an `int`. If the `id` is a type of UUID, we do not have to convert it and leave it to be a string.

Next, determine the product using the productID. Normally this is done in the database. Afterwards, create a series of switch-case statement to determine the http method. In this case there's two methods: `GET` and `PUT`.

For a GET request, all we need to do is to marshal the struct, set the `Content-type` to `application/json`, and send it using the response writer

    productJSON, err := json.Marshal(product)
    responseWriter.Header().Set("Content-Type", "application/json")
    responseWriter.Write(productJSON)

For the PUT request, we need to do the following:

(1) Read the request body using the ioutil.ReadAll method

    bodyBytes, _ := ioutil.ReadAll(request.Body)

(2) Unmarshal the request body into the product type

    var updateProduct Product
    _ := json.Unmarshal(bodyBytes, &updateProduct)

(3) Update the productList with the update product. This is normally done in the database

    product := &updateProduct
    productList[listItemIndex] = *product

(4) Return with a http.StatusOK

The code will look like this:

```go
:
func productHandler(responseWriter http.ResponseWriter, request *http.Request) {
	urlPathSegments := strings.Split(request.URL.Path, "products/")
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		responseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	product, listItemIndex := findProductByID(productID)
	if product == nil {
		responseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	switch request.Method {
	case http.MethodGet:
		productJSON, err := json.Marshal(product)
		if err != nil {
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseWriter.Header().Set("Content-Type", "application/json")
		responseWriter.Write(productJSON)
		return
	case http.MethodPut:
		var updateProduct Product
		bodyBytes, err := ioutil.ReadAll(request.Body)
		if err != nil {
			responseWriter.WriteHeader(http.StatusBadRequest)
			return
		}
		err = json.Unmarshal(bodyBytes, &updateProduct)
		if err != nil {
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		if updateProduct.ProductID != productID {
			responseWriter.WriteHeader(http.StatusBadRequest)
			return
		}
		product = &updateProduct
		productList[listItemIndex] = *product
		responseWriter.WriteHeader(http.StatusOK)
		return
	default:
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}
:
```

### Middleware

Simple diagram of where middlewares are place in a typical client-server architecture

    client -> server -> http mux -> [list of Middlewares] -> handlers

Middlewares a basically functionality that is executed before or after the intended handers are called. One way that we can implement this is by wrapping the handler in a special kind of adapter.

A middleware handler takes in an http.Handler parameter and returns also an http.Handler function.

```go
func middlewareHandler(handler http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.RequestWriter, r *http.Response){
        // add here anything BEFORE the handler
        handler.ServeHTTP(w, r)
        // add here anything AFTER the handler
    })
}
```

Next we need to convert the productsHandler and productHandler function into an http.HandlerFunc and change the routes from http.HandlerFunc to http.Handle function

```go
func main() {
    productListHandler := http.HandlerFunc(productsHandler)
    productItemHandler := http.HandlerFunc(productHandler)
    http.Handle("/products", middlewareHandler(productListHandler))
    http.Handle("/products/", middlewareHandler(productItemHandler))
}
```

For the complete code, see below

```go
:
func middlewareHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
		fmt.Println("Before handler; middleware start")
		start := time.Now()
		handler.ServeHTTP(responseWriter, request)
		fmt.Printf("middleware finished; %s", time.Since(start))
	})
}

func main() {

	productListHandler := http.HandlerFunc(productsHandler)
	productItemHandler := http.HandlerFunc(productHandler)

	http.Handle("/products", middlewareHandler(productListHandler))
	http.Handle("/products/", middlewareHandler(productItemHandler))
	http.ListenAndServe(":5000", nil)
}
:
```

### Enabling CORS

To allow any request we need to add the following to the server

    responseWriter.Header().Add("Access-Control-Allow-Origin", "*") // Allow any origin
    responseWriter.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    responseWriter.Header().Add("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Authorization, X-CSRF-Token, Accept-Encoding")

### Organizing code into packages

Product handler

- GET /products
- GET /products/{productID}
- POST /products
- PUT /products/{productID}
- DELETE /products/{productID}

Add CORS as a middleware function handler

#### Separate the data to its own json file

Create a products.json file. This is a temporary location just to hold sample data

```json
[
  {
    "productId": 1,
    "manufacturer": "Big Box Company",
    "sku": "4561qHJK",
    "upc": "414654444566",
    "pricePerUnit": "12.99",
    "quantityOnHand": 28,
    "productName": "gizmo"
  },
  {
    "productId": 2,
    "manufacturer": "Johns-Jenkins",
    "sku": "qHJKdafH",
    "upc": "8464849245",
    "pricePerUnit": "457.99",
    "quantityOnHand": 9703,
    "productName": "sticky note"
  },
  {
    "productId": 3,
    "manufacturer": "Hessel, Schimmel and Feeney",
    "sku": "976HGDB019",
    "upc": "2385098788",
    "pricePerUnit": "278.09",
    "quantityOnHand": 5908,
    "productName": "lamp shade"
  }
]
```

Next we put all product related functionality into its own package

    $ mkdir product;\
    $ touch product/product.go ;\
    $ touch product/product.server.go

Place the product definition in this `product.go` file

```go
type Product struct {
	ProductID      int    `json:"productId"`
	Manufacturer   string `json:"manufacturer"`
	Sku            string `json:"sku"`
	Upc            string `json:"upc"`
	PricePerUnit   string `json:"pricePerUnit"`
	QuantityOnHand int    `json:"quantityOnHand"`
	ProductName    string `json:"productName"`
}
```

Next we'll create the functionality to retrieve and create products in this `product.data.go` file

```go
package product

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"sync"
)

var productMap = struct {
	sync.RWMutex
	m map[int]Product
}{m: make(map[int]Product)}

func init() {
	fmt.Println("Loading products...")
	prodMap, err := loadProductMap()
	productMap.m = prodMap
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%d products loaded... \n", len(productMap.m))
}

func loadProductMap() (map[int]Product, error) {
	filename := "products.json"
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return nil, fmt.Errorf("file [%s] does not exist", filename)
	}

	file, _ := ioutil.ReadFile(filename)
	productList := make([]Product, 0)
	err = json.Unmarshal([]byte(file), &productList)
	if err != nil {
		log.Fatal(err)
	}
	prodMap := make(map[int]Product)
	for i := 0; i < len(productList); i++ {
		prodMap[productList[i].ProductID] = productList[i]
	}

	return prodMap, nil

}

func getProduct(productID int) *Product {
	productMap.RLock()
	defer productMap.RUnlock()
	if product, ok := productMap.m[productID]; ok {
		return &product
	}
	return nil
}

func removeProduct(productID int) {
	productMap.Lock()
	defer productMap.Unlock()
	delete(productMap.m, productID)
}

func getProductList() []Product {
	productMap.Lock()
	defer productMap.Unlock()
	products := make([]Product, 0, len(productMap.m))
	for _, value := range productMap.m {
		products = append(products, value)
	}
	return products
}

func getProductIds() []int {
	productMap.Lock()
	defer productMap.Unlock()
	productIds := []int{}
	for key := range productMap.m {
		productIds = append(productIds, key)
	}
	sort.Ints(productIds)
	return productIds
}

func getNextProductID() int {
	productIDs := getProductIds()
	return productIDs[len(productIDs)-1] + 1
}

func addOrUpdateProduct(product Product) (int, error) {
	addOrUpdateID := -1
	if product.ProductID > 0 {
		oldProduct := getProduct(product.ProductID)
		//if it exists, replace it, otherwise return error
		if oldProduct == nil {
			return 0, fmt.Errorf("product id [%d] does not exists", product.ProductID)
		}
		addOrUpdateID = product.ProductID
	} else {
		addOrUpdateID = getNextProductID()
		product.ProductID = addOrUpdateID
	}
	productMap.Lock()
	defer productMap.Unlock()
	productMap.m[addOrUpdateID] = product
	return addOrUpdateID, nil
}
```

Next we place the handlers into this `product.service.go` file

```go
package product

import (
	"encoding/json"
	"fmt"
	"gitlab.com/nbugash/hockeycard/cors"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const productsPath = "products"

func handleProducts(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case http.MethodGet:
		productList := getProductList()
		productsJSON, err := json.Marshal(productList)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			log.Fatal(err)
		}
		writer.Header().Set("content-type", "application/json")
		_, err = writer.Write(productsJSON)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var newProduct Product
		err := json.NewDecoder(request.Body).Decode(&newProduct)
		if err != nil {
			log.Print(err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = addOrUpdateProduct(newProduct)
		if err != nil {
			log.Print(err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		// client cannot set the PRODUCTID
		// it should be handled by the server
		if newProduct.ProductID != 0 {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		writer.WriteHeader(http.StatusCreated)
		return
	case http.MethodOptions:
		return
	default:
		writer.WriteHeader(http.StatusNotImplemented)
		return
	}
}

func handleProduct(responseWriter http.ResponseWriter, request *http.Request) {
	urlPathSegments := strings.Split(request.URL.Path, fmt.Sprintf("%s/", productsPath))
	if len(urlPathSegments[1:]) > 1 {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		responseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	switch request.Method {
	case http.MethodGet:
		product := getProduct(productID)
		if product == nil {
			responseWriter.WriteHeader(http.StatusNotFound)
			return
		}
		productJSON, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseWriter.Header().Set("Content-Type", "application/json")
		_, err = responseWriter.Write(productJSON)
		if err != nil {
			log.Fatal(err)
		}
		return
	case http.MethodPut:
		var updateProduct Product
		err := json.NewDecoder(request.Body).Decode(&updateProduct)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = addOrUpdateProduct(updateProduct)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseWriter.WriteHeader(http.StatusOK)
		return
	case http.MethodDelete:
		removeProduct(productID)
		return
	case http.MethodOptions:
		return
	default:
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

//SetupRoutes : For setting up the routes for the product handler
func SetupRoutes(apiBasePath string) {
	productsHandler := http.HandlerFunc(handleProducts)
	productHanlder := http.HandlerFunc(handleProduct)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, productsPath), cors.Middleware(productsHandler))
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, productsPath), cors.Middleware(productHanlder))
}
```

Lastly we'll update the `main.go` to use the `product.service.go`

```go
package main

import (
	"gitlab.com/nbugash/hockeycard/product"
	"log"
	"net/http"
)

const basePath = "/api"
func main() {

	product.SetupRoutes(basePath)
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
```

## Persisting Data

### Database setup

Use docker to create a mysql database. The repo contains a products.sql file under `api`.

### Connecting to a database

First, create a new package called `database` and under that directory create a `database.go` file

    mkdir -p database ;\
    touch database/database.go

Next create a public variable (a variable that can be exported) of type `*sql.DB`

```go
package database

import (
    "database/sql"
)

var DbConn *sql.DB
```

Next create a public function (a function that can be access anywhere) that sets up the database connection

```go
:

func SetupDatabase() {
    var err error
    DbConn, err = sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/inventorydb") // the signature to open a connection is sql.Open([driver name], [database connection string])
                                                                                     // the db connection string is in the format: USER:PASSWORD@tcp(HOSTNAME:PORT)/DATABASENAME
    if err != nil {
        log.Fatal(err)
    }
}
```

Next we need to download the mysql database driver since it is not included in the go standard library

    go get -u github.com/go-sql-driver/mysql

Note: You can verify that it has been downloaded by looking at the go.mod file

You can import the driver in the `main.go` file

```go
package main

import (
    _ "github.com/go-sql-driver/mysql"
	"gitlab.com/nbugash/hockeycard/product"
	"log"
	"net/http"
)

const basePath = "/api"
func main() {

	product.SetupRoutes(basePath)
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
```

### Querying data

There are two main ways to query data from the database.

(1) is to use the DB.Query method

    // this will return one of more rows
    func (db *DB) Query(query string, args interface{})(*Rows, error)

After getting the `Rows` type, we can perform the DB.Scan method to retrieve the data.

    func (rs *Rows) Scan(dest interface{}) error

A full example to use db.Query and db.Scan

    results, _ := db.Query(`select id, name from products`)
    defer results.Close() // need to close the connection
    products = make([]Product, 0) // create a slice of type Product
    for results.Next() {
        var product Product
        results.Scan(&product.id, &product.name)
        products = append(products, product) // append product to the products slice
    }

Note: The names that we pass in the results.Scan method must be the same type and order as our `SELECT` statement

(2) is to use the DB.QueryRow method

	func (db *DB) QueryRow(query string, args interface{}) *Row

This will return a single row from the database. If it happends that the data return are more than 1, it'll only get the first one and discard the rest

### Executing SQL

Next we're going to perform `INSERT`, `UPDATE`, and `DELETE` to the database. We're going to use the `DB.Exec` function

    func (db *DB) Exec(query string, args interface{}) (Result, error)

The Result interface has the following functions

```go
type Result struct {
	LastInsertId() (int64, error)
	RowsAffected() (int64, error)
}
```

To insert a record

    result, err := database.DbConn.Exec(`
    	INSERT INTO products (manufacturer,
    		sku,
    		upc,
    		pricePerUnit,
    		quantityOnHand,
    		productName) VALUES (?,?,?,?,?,?)`,
    	product.Manufacturer,
    	product.Sku,
    	product.Upc,
    	product.PricePerUnit,
    	product.QuantityOnHand,
    	product.ProductName)

To update a record

    _, err := database.DbConn.Exec(`UPDATE products SET
    		manufacturer = ?,
    		sku = ?,
    		upc = ?,
    		pricePerUnit = ?,
    		quantityOnHand = ?,
    		productName = ?
    	WHERE productId = ?`,
    	product.Manufacturer,
    	product.Sku,
    	product.Upc,
    	product.PricePerUnit,
    	product.QuantityOnHand,
    	product.ProductName,
    	product.ProductID)

To delete a record

    _, err := database.DbConn.Exec(`DELETE FROM products WHERE productId = ?`, productID)

### Connection Pool

These are the settings that are visible for configuration

- **Connection Max Life time** - sets the maximum amount of time a connection may be used
- **Max Idle connections** - sets the maximum number of connections in the idle connection pool (`Default: 2`)
- **Max Open Connections** - set the maximum number of open connections to the database. If this is set to be lower than the `Max Idle connections` settings, then it will set that setting the same value as this setting

**Context**: Allows you to set a deadline, cancel a signal, or set other request-scoped value across API boundaries and between processes.
One of the things we can set is the timeout to the database so that if it exceed the timeout value, we can cancel the request

List of Context

- QueryContext
- QueryRowContext
- ExecContext

We can create a context by doing the following

```go
timeoutDuration := 15 * time.Second
ctx, cancel := context.WithTimeout(context.Background(), timeoutDuration)
defer cancel()
```

Then we can use this `ctx` variable inside the `QueryContext`, `QueryRowContext`, and `ExecContext` functions

### Uploading and Downloading files

There are two ways to handle files in a rest api server

(1) is to decode the file as a base64 encoded string and setting that string as one of the fields in the json body in the request

(2) Use the http multipart form data content type

    func (r *Request) FormFile(key string) (multipart.File, *multipart.FileHeader, error)

    //Allows us to read the file contents
    type File struct {
    	io.Reader
    	io.ReaderAt
    	io.Seeker
    	io.Closer
    }

    //Contains information about the file
    type FileHeader struct {
    	Filename string
    	Header textproto.MIMEHeader
    	Size int64
    }

To **upload** file to the server

```go
func uploadFileHandler(res http.ResponseWriter, req *http.Request) {
	req.ParseMultiPartForm(5 << 20) // 5 Mb
	file, fileHandler, err := req.FormFile("uploadedFilename") // required key
	defer file.Close()
	f, err := os.OpenFile("./filepath", fileHandler.Filename, os.O_WRONLY|os.O_CREATE, 0666) // create a new file at a given file path
	defer f.Close()
	io.Copy(f, file) // using this lib to copy the byte data that we read in from the request to the new file
}
```

Note: If the size of the data is bigger than the limit - i.e 20 Mb, it will store the data temporarily on disk

To **download** the file from the server

```go
func downloadFileHandler(res http.ResponseWriter, req *http.Request) {
	filename := "filename.txt"
	file, err := os.Open(filename)
	defer file.Close()
	res.Header.Set("Content-Disposition", "attachment: filename=" + filename)
	io.Copy(res, file)
}
```

Last thing is to set up the `uploadFileHandler` and the `downloadFileHandler`

```go
//SetupRoutes : For setting up the routes for the receipt handler
func SetupRoutes(apiBasePath string) {
	receiptHandler := http.HandlerFunc(handleReceipts)
	downloadHandler := http.HandlerFunc(handleDownload)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, receiptPath), cors.Middleware(receiptHandler))
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, receiptPath), cors.Middleware(downloadHandler))
}
```

## Using Websockets

### Websockets

### Creating websockets in Go

## Using Templating

### Templating Basics

### Pipelines

### Looping

### Global functions and operators

### Custom functions
