package product

import (
	"encoding/json"
	"fmt"
	"gitlab.com/nbugash/golang-restapi/cors"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const productsPath = "products"

func handleProducts(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case http.MethodGet:
		productList, err := getProductList()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			log.Fatal(err)
			return
		}
		productsJSON, err := json.Marshal(productList)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			log.Fatal(err)
		}
		writer.Header().Set("content-type", "application/json")
		_, err = writer.Write(productsJSON)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var newProduct Product
		err := json.NewDecoder(request.Body).Decode(&newProduct)
		if err != nil {
			log.Print(err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = insertProduct(newProduct)
		if err != nil {
			log.Print(err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		// client cannot set the PRODUCTID
		// it should be handled by the server
		if newProduct.ProductID != 0 {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		writer.WriteHeader(http.StatusCreated)
		return
	case http.MethodOptions:
		return
	default:
		writer.WriteHeader(http.StatusNotImplemented)
		return
	}
}

func handleProduct(responseWriter http.ResponseWriter, request *http.Request) {
	urlPathSegments := strings.Split(request.URL.Path, fmt.Sprintf("%s/", productsPath))
	if len(urlPathSegments[1:]) > 1 {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		responseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	switch request.Method {
	case http.MethodGet:
		product, err := getProduct(productID)
		if err != nil {
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			responseWriter.WriteHeader(http.StatusNotFound)
			return
		}
		productJSON, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseWriter.Header().Set("Content-Type", "application/json")
		_, err = responseWriter.Write(productJSON)
		if err != nil {
			log.Fatal(err)
		}
		return
	case http.MethodPut:
		var product Product
		err := json.NewDecoder(request.Body).Decode(&product)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusBadRequest)
			return
		}
		err = updateProduct(product)
		if err != nil {
			log.Print(err)
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseWriter.WriteHeader(http.StatusOK)
		return
	case http.MethodDelete:
		err := removeProduct(productID)
		if err != nil {
			responseWriter.WriteHeader(http.StatusNotFound)
			return
		}
		return
	case http.MethodOptions:
		return
	default:
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

//SetupRoutes : For setting up the routes for the product handler
func SetupRoutes(apiBasePath string) {
	productsHandler := http.HandlerFunc(handleProducts)
	productHanlder := http.HandlerFunc(handleProduct)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, productsPath), cors.Middleware(productsHandler))
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, productsPath), cors.Middleware(productHanlder))
}
