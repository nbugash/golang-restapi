package main

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/nbugash/golang-restapi/database"
	"gitlab.com/nbugash/golang-restapi/product"
	"gitlab.com/nbugash/golang-restapi/receipt"
	"log"
	"net/http"
)

const basePath = "/api"

func main() {
	database.SetupDatabase()
	product.SetupRoutes(basePath)
	receipt.SetupRoutes(basePath)
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
